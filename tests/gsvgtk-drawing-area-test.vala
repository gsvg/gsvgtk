/*
 * gsvg-ellipse-test.vala

 * Copyright (C) 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GSvgtk;
using Gtkt;

class GSvgTest.Suite : GLib.Object
{
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Gtk.init ();
        Test.init (ref args);
        Test.add_func ("/gsvgtk/drawing-area/update-image",
        ()=>{
            var app = new Gtkt.Application ();
            var w = new GSvgtk.DrawingAreaGtk ();
            var svg1 = new GSvg.Document ();
            var svg2 = new GSvg.Document ();
            var f1 = GLib.File.new_for_uri ("file://"+ConfigTest.TEST_DIR+"/one-icon.svg");
            var f2 = GLib.File.new_for_uri ("file://"+ConfigTest.TEST_DIR+"/shapes.svg");
            message (f1.get_path ());
            assert (f1.query_exists ());
            message (f2.get_path ());
            assert (f2.query_exists ());
            app.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
            app.add_test ("Set-Image", "Set First Image");
            app.add_test ("Update-Image", "Set Second Image");
            app.initialize_widget.connect (()=>{
                app.set_widget (w);
            });

            app.initialize.connect (()=>{
                message ("%d", app.current_ntest);
                switch (app.current_ntest) {
                    case 1:
                        try {
                            svg1.read_from_file (f1);
                            message ("Doc1:\n%s", svg1.write_string ());
                            w.replace_svg (svg1);
                        } catch (GLib.Error e) {
                            warning ("Error: "+e.message);
                        }
                        break;
                    case 2:
                        try {
                            svg2.read_from_file (f2);
                            message ("Doc2:\n%s", svg2.write_string ());
                            w.replace_svg (svg2);
                        } catch (GLib.Error e) {
                            warning ("Error: "+e.message);
                        }
                        break;
                }
            });

            app.finished.connect (()=>{
                message ("Finishing tests");
                if (app.active_window != null) {
                    app.active_window.destroy ();
                }
                app.quit ();
            });

            app.run ();
        });
        return Test.run ();
    }
}


