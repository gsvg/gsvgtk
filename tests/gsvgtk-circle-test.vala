/*
 * gsvg-item-test.vala

 * Copyright (C) 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GSvgtk;
using Gtkt;

class GSvgTest.Suite : GLib.Object
{
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Gtk.init ();
        Test.init (ref args);
        Test.add_func ("/gsvgtk/circle",
        ()=>{
            var app = new Gtkt.Application ();
            app.initialize_widget.connect (()=>{
                try {
                    var f = GLib.File.new_for_uri ("file://"+ConfigTest.TEST_DIR+"/shapes.svg");
                    message (f.get_path ());
                    assert (f.query_exists ());
                    var ssvg = new GSvg.Document ();
                    ssvg.read_from_file (f);
                    var cr = ssvg.get_element_by_id ("ellipse");
                    assert (cr != null);
                    message (((GXml.Element) cr).write_string ());
                    var a = new GSvgtk.Rect.from_id (ssvg, "ellipse");
                    var svg = new GSvg.Document ();
                    svg.read_from_string (a.svg.write_string (), g ())
                    var w = new GSvgtk.DrawingAreaGtk ();
                    w.svg = svg;
                    app.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
                    app.add_test ("Init", "Showing a yellow filled circle with line in red");
                    app.set_widget (w);
                    message (a.element.write_string ());
                    message (a.fake_doc.write_string ());
                } catch (GLib.Error e) {
                    warning ("Error: "+e.message);
                }
            });

            app.initialize.connect (()=>{
            });

            app.finished.connect (()=>{
                message ("Finishing tests");
                app.quit ();
            });

            app.run ();
        });

        return Test.run ();
    }
}


