/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-actor-circle-test.vala

 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GSvgtk;
using Gtkt;
using GXml;

class GSvgTest.Suite : GLib.Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Gtk.init ();
    Test.init (ref args);
    Test.add_func ("/gsvgtk/renderer/color",
    ()=>{
      try {
        string color = "red";
        double r, g, b, a;
        r = g = b = a = 0.0;
        SvgRenderer.parse_color_rgba (color, out r, out g, out b, out a);
        message ("(%s)=>(%g,%g,%g,%g)", color, r, g, b, a);
        assert (r == 1);
        assert (g == 0);
        assert (b == 0);
        assert (a == 1);
        color = "#ffffffff";
        SvgRenderer.parse_color_rgba (color, out r, out g, out b, out a);
        message ("(%s)=>(%g,%g,%g,%g)", color, r, g, b, a);
        assert (r == 1);
        assert (g == 1);
        assert (b == 1);
        assert (a == 1);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    return Test.run ();
  }
}
