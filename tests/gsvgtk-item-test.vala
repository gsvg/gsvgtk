/*
 * gsvg-item-test.vala

 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GSvgtk;
using Gtkt;

class GSvgTest.Suite : GLib.Object
{
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Gtk.init ();
        Test.init (ref args);
        Test.add_func ("/gsvgtk/item",
        ()=>{
            var a = new GSvgtk.Item ();
            var w = new GSvgtk.DrawingAreaGtk (a);
            var app = new Gtkt.Application ();
            app.initialize_widget.connect (()=>{
                app.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
                app.add_test ("Init", "Showing one icon");
                app.set_widget (w);
            });

            app.initialize.connect (()=>{
                try {
                    var f = GLib.File.new_for_uri ("file://"+ConfigTest.TEST_DIR+"/one-icon.svg");
                    message (f.get_path ());
                    assert (f.query_exists ());
                    var svg = new GSvg.Document ();
                    svg.read_from_file (f);
                    a.svg = svg;
                } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                }
            });

            app.finished.connect (()=>{
                message ("Finishing tests");
                app.quit ();
            });

            app.run ();
        });
        Test.add_func ("/gsvgtk/item/shapes",
        ()=>{
            var a = new GSvgtk.Item ();
            var w = new GSvgtk.DrawingAreaGtk (a);
            var app = new Gtkt.Application ();
            app.initialize_widget.connect (()=>{
                app.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
                app.add_test ("Init", "Showing shapes");
                app.set_widget (w);
            });

            app.initialize.connect (()=>{
                try {
                    var f = GLib.File.new_for_uri ("file://"+ConfigTest.TEST_DIR+"/shapes.svg");
                    message (f.get_path ());
                    assert (f.query_exists ());
                    var svg = new GSvg.Document ();
                    svg.read_from_file (f);
                    a.svg = svg;
                } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                }
            });

            app.finished.connect (()=>{
                message ("Finishing tests");
                app.quit ();
            });

            app.run ();
        });

        return Test.run ();
    }
}


