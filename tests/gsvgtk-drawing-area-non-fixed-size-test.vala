/*
 * gsvg-ellipse-test.vala

 * Copyright (C) 2022-2024 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GSvgtk;
using Gtkt;

class GSvgTest.Suite : GLib.Object
{
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Gtk.init ();
        Test.init (ref args);
        Test.add_func ("/gsvgtk/drawing-area/non-fixed-size",
        ()=>{
            var app = new Gtkt.Application ();
            app.initialize_widget.connect (()=>{
                try {
                    var f = GLib.File.new_for_uri ("file://"+ConfigTest.TEST_DIR+"/shapes.svg");
                    message (f.get_path ());
                    assert (f.query_exists ());
                    var svg = new GSvg.Document ();
                    svg.read_from_file (f);
                    message ("Doc:\n%s", svg.write_string ());
                    var w = new GSvgtk.DrawingAreaGtk ();
                    w.replace_svg (svg);
                    w.fixed_size = false;
                    message ("Doc:\n%s", w.viewer.write_string ());
                    app.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
                    app.add_test ("Init", "Non Fixed default size");
                    app.set_widget (w);
                } catch (GLib.Error e) {
                    warning ("Error: "+e.message);
                }
            });

            app.initialize.connect (()=>{
            });

            app.finished.connect (()=>{
                message ("Finishing tests");
                if (app.active_window != null) {
                    app.active_window.destroy ();
                }
                app.quit ();
            });

            app.run ();
        });

        return Test.run ();
    }
}
