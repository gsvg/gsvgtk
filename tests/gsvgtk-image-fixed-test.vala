/*
 * gsvg-image-fixed-test.vala

 * Copyright (C) 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GSvgtk;
using Gtkt;

class GSvgTest.Suite : GLib.Object
{
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Test.init (ref args);
        Gtk.init ();
        Test.add_func ("/gsvgtk/image/init",
        ()=>{
            var w = new GSvgtk.ImageFixed ();
            var app = new Gtkt.Application ();
            app.initialize_widget.connect (()=>{
                app.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
                app.add_test ("Init", "Showing an SVG");
                app.set_widget (w);
            });

            app.initialize.connect (()=>{
                try {
                    var f = GLib.File.new_for_uri ("file://"+ConfigTest.TEST_DIR+"/image-test-1.svg");
                    message (f.get_path ());
                    assert (f.query_exists ());
                    var svg = new GSvg.Document ();
                    svg.read_from_file (f);
                    w.svg = svg;
                } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                }
            });

            app.finished.connect (()=>{
                message ("Finishing tests");
                app.quit ();
            });

            app.run ();
        });
        Test.add_func ("/gsvgtk/image/size-change",
        ()=>{
            var w = new GSvgtk.ImageFixed ();
            var app = new Gtkt.Application ();
            app.initialize_widget.connect (()=>{
                app.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
                app.add_test ("Init", "Showing an SVG");
                app.set_widget (w);
            });

            app.initialize.connect (()=>{
                try {
                    var f = GLib.File.new_for_uri ("file://"+ConfigTest.TEST_DIR+"/image-test-1.svg");
                    message (f.get_path ());
                    assert (f.query_exists ());
                    var svg = new GSvg.Document ();
                    svg.read_from_file (f);
                    w.svg = svg;
                    w.set_size (400,200);
                } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                }
            });

            app.finished.connect (()=>{
                message ("Finishing tests");
                app.quit ();
            });

            app.run ();
        });
        Test.add_func ("/gsvgtk/image/invalid",
        ()=>{
            var w = new GSvgtk.ImageFixed ();
            var app = new Gtkt.Application ();
            app.initialize_widget.connect (()=>{
                app.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
                app.add_test ("Init", "Showing an invalid SVG");
                app.set_widget (w);
            });

            app.initialize.connect (()=>{
                try {
                    var svg = new GSvg.Document ();
                    svg.read_from_string ("");
                    w.svg = svg;
                } catch (GLib.Error e) {
                    message ("Catched correctly error: "+e.message);
                }
            });

            app.finished.connect (()=>{
                message ("Finishing tests");
                app.quit ();
            });

            app.run ();
        });
        Test.add_func ("/gsvgtk/image/four-icons",
        ()=>{
            var w = new GSvgtk.ImageFixed ();
            var app = new Gtkt.Application ();
            app.initialize_widget.connect (()=>{
                app.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
                app.add_test ("Init", "Showing four icons");
                app.set_widget (w);
            });

            app.initialize.connect (()=>{
                try {
                    var f = GLib.File.new_for_uri ("file://"+ConfigTest.TEST_DIR+"/four-icon.svg");
                    message (f.get_path ());
                    assert (f.query_exists ());
                    var svg = new GSvg.Document ();
                    svg.read_from_file (f);
                    w.svg = svg;
                } catch (GLib.Error e) {
                    warning ("Error: "+e.message);
                }
            });

            app.finished.connect (()=>{
                message ("Finishing tests");
                app.quit ();
            });

            app.run ();
        });
        Test.add_func ("/gsvgtk/image/transform/custom",
        ()=>{
            var w = new GSvgtk.ImageFixed ();
            var app = new Gtkt.Application ();
            app.initialize_widget.connect (()=>{
                app.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
                app.add_test ("Transform", "Rotate 30 degrees");
                app.set_widget (w);
            });

            app.initialize.connect (()=>{
                try {
                    var f = GLib.File.new_for_uri ("file://"+ConfigTest.TEST_DIR+"/image-test-1.svg");
                    message (f.get_path ());
                    assert (f.query_exists ());
                    var svg = new GSvg.Document ();
                    svg.read_from_file (f);
                    w.svg = svg;
                    w.add_transformation ("rotate(30)");
                } catch (GLib.Error e) {
                    warning ("Error: "+e.message);
                }
            });

            app.finished.connect (()=>{
                message ("Finishing tests");
                app.quit ();
            });

            app.run ();
        });
        Test.add_func ("/gsvgtk/image/transform/scale/0.5",
        ()=>{
            var w = new GSvgtk.ImageFixed ();
            var app = new Gtkt.Application ();
            app.initialize_widget.connect (()=>{
                app.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
                app.add_test ("Transform", "Scale 0.5");
                app.set_widget (w);
            });

            app.initialize.connect (()=>{
                try {
                    var f = GLib.File.new_for_uri ("file://"+ConfigTest.TEST_DIR+"/image-test-1.svg");
                    message (f.get_path ());
                    assert (f.query_exists ());
                    var svg = new GSvg.Document ();
                    svg.read_from_file (f);
                    w.svg = svg;
                    w.set_size (200, 200);
                    w.scale (0.5);
                } catch (GLib.Error e) {
                    warning ("Error: "+e.message);
                }
            });

            app.finished.connect (()=>{
                message ("Finishing tests");
                app.quit ();
            });

            app.run ();
        });
        return Test.run ();
    }
}
