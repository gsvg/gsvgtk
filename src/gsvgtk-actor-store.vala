/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor-store.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gee;

public interface GSvgtk.ActorStore : Object, GLib.ListModel
{
  /**
   * Search a child actor in the store
   * with the given 'id'.
   */
  public abstract Actor? find (string id);
  /**
   * Adds a child actor in the store
   * with the given 'id'.
   */
  public abstract void add (string id, Actor actor);
  /**
   * Removes a child actor in the store
   * with the given 'id'.
   */
  public abstract void remove (string id);
}

