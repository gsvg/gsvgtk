/* gsvgtk-drawing-area.vala
 *
 * Copyright (C) 2017-2024 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;

public class GSvgtk.DrawingAreaGtk : Gtk.DrawingArea, GSvgtk.DrawingArea {
      private double x_pmm;
      // private double y_pmm;
      private Gtk.GestureClick _press_gesture;
      private Gtk.GestureDrag _drag_gesture;

      internal GLib.ListModel items { get; set; }

      internal GSvg.DomDocument svg { get; internal set; }
      internal GSvg.DomDocument viewer { get; internal set; }
      internal GSvg.DomSvgElement container { get; internal set; }
      internal GLib.File? file { get; internal set; }
      internal GSvgtk.SvgRenderer renderer { get; set; }

      internal double ppmm { get { return x_pmm; } }
      internal double width_mm { get; set; }
      internal double height_mm { get; set; }
      internal bool fixed_size { get; set; }
      public Gtk.GestureClick      press_gesture { get { return _press_gesture; } }
      public Gtk.GestureDrag       drag_gesture { get { return _drag_gesture; } }

    construct {
        hexpand = true;
        vexpand = true;
        width_mm = -1;
        height_mm = -1;
        fixed_size = true;
        content_width = 100 * 10;
        content_height = 100 * 10;
        renderer = new GSvgtk.SvgRendererRsvg ();
        _press_gesture = new Gtk.GestureClick ();
        add_controller (_press_gesture);
        _drag_gesture = new Gtk.GestureDrag ();
        add_controller (_drag_gesture);
        set_draw_func ((da, ctx, w, h)=>{
        var ct = new ContextCairo (ctx);
            draw_svg (ct, w, h);
        });
    }

    internal void invalidate () {
        queue_draw ();
    }

    internal void set_drawing_size (int width, int  height) {
            set_content_height (height);
            set_content_width (width);
    }

      internal void get_drawing_size (out int width, out int  height) {
            width = get_width ();
            height = get_height ();
      }
}
