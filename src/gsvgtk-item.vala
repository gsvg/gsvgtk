/* gsvgtk-actor.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;
using Gtk;
using GXml;
using Cairo;
using Gee;

public class GSvgtk.Item : GLib.Object,
                              GSvgtk.ActorResizable,
                              GSvgtk.Positionable,
                              GLib.ListModel,
                              GSvgtk.ActorStore,
                              GSvgtk.Actor
{
    protected GSvgtk.DrawingArea _drawing_area;
    protected GSvg.DomDocument _svg = null;
    GSvg.DomSvgElement current = null;
    ActorStore _actors = new ActorStoreContainer ();
    weak Actor _parent = null;

    public Item.with_parent (string id, Actor parent) {
        GLib.Object (id: id, parent: parent, drawing_area: parent.drawing_area);
        _parent.add (id, this);
    }

        // GSvgtk.Actor implementation
    protected GSvg.DomElement _element;
    protected GSvg.DomSvgElement root;
    public GSvg.DomDocument fake_doc { get; internal set; }
    public GSvg.DomElement mirror_element { get; internal set; }
    public GSvg.DomElement mirror_gelement { get; internal set; }
    public GSvg.DomElement element {
        get {
            return _element;
        }
        construct set {
            _element = value;
            if (_element != null) {
                mirror_element = (GSvg.DomElement) GLib.Object.new (_element.get_type (),
                                                "owner-document", fake_doc);
                try {
                    mirror_element.read_from_string (_element.write_string ());
                    mirror_gelement.append_child (mirror_element);
                    if (drawing_area != null) {
                        drawing_area.invalidate ();
                    }
                } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                }
            }
        }
    }
    internal GSvg.DomDocument svg {
        get { return _svg; }
        set {
            _svg = value;
            var re = _svg.root_element;
            if (re != null) {
                var d = new GSvg.Document ();
                editor.modifications = d.root_element;
                try {
                    editor.modifications.set_attribute ("width", "0");
                    editor.modifications.set_attribute ("height", "0");
                    editor.modifications.width.base_val.value = width_px;
                    editor.modifications.height.base_val.value = height_px;
                    editor.modifications.id = "@gsvgtk-modifications@";
                    current = editor.modifications.add_svg (null, null, null, null, null, null, null);
                    current.read_from_string (re.write_string ());
                } catch (GLib.Error e) {
                    warning ("Error while initialize modifications SVG: %s", e.message);
                }

                drawing_area.svg = d;
                drawing_area.invalidate ();
            }
        }
    }
    public SvgRenderer renderer { get; set; }
    public GSvgtk.DrawingArea drawing_area {
        get { return _drawing_area; }
        construct set {
            _drawing_area = value;
        }
    }
    public GSvgtk.Editor editor { get; internal set; }
    public bool picking { get; internal set; }
    public bool dragging { get; internal set; }
    public GSvgtk.Actor parent {
        get { return _parent; }
        construct set { _parent = value; }
    }
    public string id { get; construct set; }

    public Actor? find (string id) { return _actors.find (id); }
    public void add (string id, Actor actor) { _actors.add (id, actor); }
    public void remove (string id) { _actors.remove (id); }

    public virtual bool import_svg (GSvg.DomDocument dsvg) throws GLib.Error {
        var nactor = new Image (drawing_area, "#Imported1#", this);
        var ret = nactor.import_svg (dsvg);
        drawing_area.invalidate ();
        return ret;
    }

    // ListModel
    public GLib.Object? get_item (uint position) { return _actors.get_item (position); }
    public Type get_item_type () { return _actors.get_item_type (); }
    public uint get_n_items () { return _actors.get_n_items (); }
    public GLib.Object? get_object (uint position) { return _actors.get_item (position); }

    // GSvgtk.ActorResizable
    private double _width_mm;
    private double _height_mm;
    public double x_ppm { get; set; }
    public double y_ppm { get; set; }
    public double width_mm {
        get { return _width_mm; }
        set {
            _width_mm = value;
            if (drawing_area != null) {
                drawing_area.set_drawing_size ((int) width_px, (int) height_px);
            }
        }
    }
    public double height_mm {
        get { return _height_mm; }
        set {
            _height_mm = value;
            if (drawing_area != null) {
                drawing_area.set_drawing_size ((int) width_px, (int) height_px);
            }
        }
    }
    public float width_px { get { return (float) (x_ppm * width_mm); } }
    public float height_px { get { return (float) (y_ppm * height_mm); } }
    // GSvgtk.Positionable
    public double x_mm { get; set; }
    public double y_mm { get; set; }

    construct {
        x_ppm = 10;
        y_ppm = 10;
        width_mm = 50;
        height_mm = 50;
        renderer = new SvgRendererRsvg ();
        _drawing_area = new DrawingAreaGtk ();
        editor = new Editor (this);
        var empty = new GSvg.Document ();
        svg = empty;
        _drawing_area.svg = svg;
    }
}

