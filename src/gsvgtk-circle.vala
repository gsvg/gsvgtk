/* gsvgtk-actor-circle-dwa.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GSvgtk.Circle : GSvgtk.Shape,
                            GSvgtk.ActorCircle
{
    public Circle.from_id (GSvgtk.DrawingArea dw, GSvg.Document svg, string id) {
        drawing_area = dw;
        element = dw.extract_element_from_id (svg, id);
    }

    public Circle.from_element (GSvgtk.DrawingArea dw, GSvg.DomCircleElement circle) {
        element = circle;
    }
}
