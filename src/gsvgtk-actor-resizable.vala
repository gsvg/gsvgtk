/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor-resizable.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GSvgtk.ActorResizable : Object {
  /**
   * Pixels per milimeter in the x axis.
   *
   * Updating this property, implementators should calculate a new width
   * of the graphic object, update any other object, like renderers, and
   * call {@link GSvgtk.DrawingArea.invalidate} method, in order
   * to redraw the actor.
   *
   * Default value should be set to 10 points per milimeter
   */
  public abstract double x_ppm { get; set; }
  /**
   * Pixels per milimeter in the y axis
   *
   * Updating this property, implementators should calculate a new height
   * of the graphic object, update any other object, like renderers, and
   * call {@link GSvgtk.DrawingArea.invalidate} method, in order
   * to redraw the actor.
   *
   * Default value should be set to 10 points per milimeter
   */
  public abstract double y_ppm { get; set; }
  /**
   * Width in milimeters
   *
   * Updating this property, implementators should calculate a new width
   * of the graphic object and call {@link GSvgtk.DrawingArea.invalidate}
   * method, in order to redraw the actor.
   */
  public abstract double width_mm { get; set; }
  /**
   * Height in milimeters
   *
   * Updating this property, implementators should calculate a new height
   * of the graphic object and call {@link GSvgtk.DrawingArea.invalidate}
   * method, in order to redraw the actor.
   */
  public abstract double height_mm { get; set; }
  /**
   * Width in pixels
   *
   * This property is the actual size of the graphic object, modified by
   * {@link width_mm} and {@link x_ppm} properties.
   */
  public abstract float width_px { get; }
  /**
   * Height in pixels
   *
   * This property is the actual size of the graphic object, modified by
   * {@link height_mm} and {@link y_ppm} properties.
   */
  public abstract float height_px { get; }

  public virtual void reserved0() {}
  public virtual void reserved1() {}
  public virtual void reserved2() {}
  public virtual void reserved3() {}
  public virtual void reserved4() {}
  public virtual void reserved5() {}
  public virtual void reserved6() {}
  public virtual void reserved7() {}
  public virtual void reserved8() {}
  public virtual void reserved9() {}
}
