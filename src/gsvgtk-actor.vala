/* gsvgtk-actor.vala
 *
 * Copyright (C) 2017-2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;
using Gtk;
using GXml;
using Cairo;
using Gee;

public interface GSvgtk.Actor : GLib.Object,
                                GSvgtk.ActorResizable,
                                GSvgtk.Positionable,
                                GSvgtk.ActorStore
{

    // GSvgtk.Actor implementation
    public abstract GSvg.DomElement element { get; construct set; }
    public abstract GSvgtk.DrawingArea drawing_area { get; construct set; }
    public abstract bool picking { get; internal set; }
    public abstract GSvgtk.Actor parent { get; construct set; }
    public abstract string id { get; construct set; }

    /**
    * Picks an {@link GSvg.Element} at a given point in pixels.
    */
    public virtual GSvg.DomElement? pick (GSvg.DomPoint point) {
        if (this is ActorShape) {
            debug ("Picking a Shape");
            if (((ActorShape) this).pick_element (point)) {
                return ((ActorShape) this).element;
            }

            debug ("Fail pick a shape");
            return null;
        }

        debug ("Picking");
        if (drawing_area.svg == null) {
            return null;
        }

        if (drawing_area.svg.root_element == null) {
            return null;
        }

        debug ("Picking over root element children");
        var iter = ((BidirList<DomNode>) drawing_area.svg.root_element.child_nodes).bidir_list_iterator ();
        if (!iter.last ()) {
            return null;
        }

        while (iter.valid) {
            var n = iter.get ();
            if (n is GSvg.DomElement) {
                var e = (GSvg.DomElement) n;
                if (e.pick (point, drawing_area.ppmm / 2)) {
                    return e;
                }
            }

            if (!iter.has_previous ()) {
                return null;
            }

            iter.previous ();
        }
        return null;
    }

    // Static methods
    public static double get_dpi (double width, GSvg.DomLength.Type units) {
        double m = 92;
        switch (units) {
            case GSvg.DomLength.Type.MM:
                m = width / 25.4;
                break;
            case GSvg.DomLength.Type.CM:
                m = width / 2.54;
                break;
            case GSvg.DomLength.Type.PX:
            case GSvg.DomLength.Type.PERCENTAGE:
            case GSvg.DomLength.Type.IN:
            case GSvg.DomLength.Type.EMS:
            case GSvg.DomLength.Type.UNKNOWN:
            case GSvg.DomLength.Type.PT:
            case GSvg.DomLength.Type.EXS:
            case GSvg.DomLength.Type.NUMBER:
            case GSvg.DomLength.Type.PC:
                break;
        }
        return m;
    }
}
