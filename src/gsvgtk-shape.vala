/* gsvgtk-actor-shape.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;
using Gtk;
using GXml;
using Cairo;

public class GSvgtk.Shape : GSvgtk.Item, GSvgtk.ActorShape {

    construct {
        fake_doc = new GSvg.Document ();
        root = fake_doc.root_element;
        mirror_gelement = root.add_g ();
        mirror_gelement.id = "gsvgtk::g";
        _element = null;
        drawing_area.svg = fake_doc;
    }
}

