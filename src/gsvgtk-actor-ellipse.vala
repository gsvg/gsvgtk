/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor-ellipse.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GSvgtk.ActorEllipse : Object,
                                      GSvgtk.ActorResizable,
                                      GSvgtk.Positionable,
                                      GSvgtk.Actor,
                                      GSvgtk.ActorShape
{
  public virtual void reserved0() {}
  public virtual void reserved1() {}
  public virtual void reserved2() {}
  public virtual void reserved3() {}
  public virtual void reserved4() {}
  public virtual void reserved5() {}
  public virtual void reserved6() {}
  public virtual void reserved7() {}
  public virtual void reserved8() {}
  public virtual void reserved9() {}
}
