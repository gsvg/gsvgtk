/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-editor-dwa.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;

public class GSvgtk.Editor : GLib.Object, GSvgtk.ActorEditor {
  public GSvgtk.Changes changes { get; internal set; }
  public GSvgtk.Actor actor  { get; internal set; }
  public GSvg.DomPointList movedto { get; internal set; }
  public GSvg.DomSvgElement modifications { get; internal set; }
  public GSvg.DomSvgElement selection_box { get; internal set; }
  public GSvg.DomElement selected { get; internal set; }

  construct {
    movedto = new PointList ();
  }
  public Editor (GSvgtk.Actor actor) {
    this.actor = actor;
  }

  public void undo () {}
}
