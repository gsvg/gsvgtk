/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-svg-renderer.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;
using Cairo;
using GXml;
using Gee;

public class GSvgtk.SvgRendererCairo : GLib.Object, GSvgtk.SvgRenderer {
  internal GSvgtk.Context ctx { get; set; }
  internal GLib.File? file { get; set; }
  internal GSvg.DomDocument svg { get; set; }
  internal GLib.Cancellable? cancellable { get; set; }
  internal string id { get; set; }
  internal double ppmm { get; set; }
  internal double width { get; set; }
  internal double height { get; set; }

  internal void render (GSvgtk.Context ctx, GSvg.DomDocument svg, GLib.File? file = null, GLib.Cancellable? cancellable = null) throws GLib.Error
    requires (svg is GSvg.Document)
  {
    this.ctx = ctx;
    this.file = file;
    this.svg = svg;
    this.cancellable = cancellable;
    render_svg ();
  }
  internal void render_sized (GSvgtk.Context ctx,  double width, double height, GSvg.DomDocument svg, GLib.File? file = null, GLib.Cancellable? cancellable = null ) throws GLib.Error
    requires (svg is GSvg.Document)
  {
    render (ctx, svg, file, cancellable);
  }
  internal Gdk.Pixbuf? render_pixbuf () throws GLib.Error
    requires (svg is GSvg.Document)
  {
    double w = 100.0 * ppmm;
    double h = 100.0 * ppmm;
    if (svg.root_element != null) {
      double cw, ch;
      cw = ch = 0.0;
      svg.root_element.calculate_box (out w, out h);
      if (cw != -1 && ch != -1) {
        w = cw;
        h = ch;
      }
    }
    var surface = new Cairo.ImageSurface (Cairo.Format.ARGB32, (int) Math.round (w), (int) Math.round (h));
    var cx = new Cairo.Context (surface);
    ctx = new GSvgtk.ContextCairo (cx);
    render_svg ();
    return null;
  }
  public Gdk.Pixbuf? render_pixbuf_sized (double width, double height) throws GLib.Error
    requires (svg is GSvg.Document)
  {
    return render_pixbuf ();
  }
  public Cairo.PdfSurface render_pdf (string? filename) throws GLib.Error
    requires (svg is GSvg.Document)
  {
    double w = 100 * ppmm;
    double h = 100 * ppmm;
    if (svg.root_element != null) {
      double cw, ch;
      cw = ch = 0.0;
      svg.root_element.calculate_box (out w, out h);
      if (cw != -1 && ch != -1) {
        w = cw;
        h = ch;
      }
    }
    var surface = new PdfSurface (filename, w, h);
    var cx = new Cairo.Context (surface);
    ctx = new GSvgtk.ContextCairo (cx);
    render_svg ();
    return surface;
  }
  public void render_svg (GLib.Cancellable? cancellable = null) throws GLib.Error
    requires (svg is GSvg.Document)
  {
    message ("Rendering SVG");
    var e = svg.root_element;
    render_svg_element (e);
  }
  internal void render_svg_element (GSvg.DomSvgElement s) throws GLib.Error {
    render_container (s, s);
  }
  internal void render_g_element (GSvg.DomSvgElement s, GSvg.DomGElement g) throws GLib.Error {
    GSvg.DomSvgElement root = null;
    if (s.owner_document is GSvg.Document) {
      root = ((GSvg.Document) s.owner_document).root_element;
    }
    if (root == null) {
      return;
    }
    ctx.save ();
    transform (g, root);
    render_container (s, g);
    ctx.restore ();
  }
  internal void render_container (GSvg.DomSvgElement s, GSvg.DomContainerElement e) {
    if (!(e is GXml.DomElement)) return;
    foreach (DomNode n in ((Iterable<DomNode>) ((GXml.DomElement) e).child_nodes)) {
      message ("Node Type: "+n.get_type ().name ());
      try {
        if (n is GSvg.DomSvgElement) {
          render_svg_element ((GSvg.DomSvgElement) n);
        }
        if (n is GSvg.DomLineElement) {
          render_line_element ((GSvg.DomLineElement) n);
        }
        if (n is GSvg.DomCircleElement) {
          render_circle_element ((GSvg.DomCircleElement) n);
        }
        if (n is GSvg.DomTextElement) {
          render_text_element ((GSvg.DomTextElement) n, s);
        }
        if (n is GSvg.DomRectElement) {
          render_rect_element ((GSvg.DomRectElement) n);
        }
        if (n is GSvg.DomEllipseElement) {
          render_ellipse_element ((GSvg.DomEllipseElement) n);
        }
        if (n is GSvg.DomPolylineElement) {
          render_polyline_element ((GSvg.DomPolylineElement) n);
        }
        if (n is PolygonElement) {
          render_polygon_element ((PolygonElement) n);
        }
        if (n is GSvg.DomGElement) {
          render_g_element (s, (GSvg.DomGElement) n);
        }
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    }
  }

  internal void transform (GSvg.DomTransformable e, GSvg.DomSvgElement root) throws GLib.Error {
    if (e.transform == null) return;
    if (e.transform.base_val == null) return;
    for (int i = 0; i < e.transform.base_val.number_of_items; i++) {
      var t = e.transform.base_val.get_item (i);
      if (t.ttype == DomTransform.Type.TRANSLATE) {
        ctx.translate (t.matrix.e, t.matrix.f);
      }
      if (t.ttype == DomTransform.Type.SCALE) {
        ctx.scale (t.matrix.a, t.matrix.d);
      }
      if (t.ttype == DomTransform.Type.ROTATE) {
        if (t.rotation_center != null) {
          double x = root.convert_length_x_pixels (t.rotation_center.x);
          double y = root.convert_length_y_pixels (t.rotation_center.y);
          ctx.translate (x,y);
          ctx.rotate (t.angle);
          ctx.translate (-x,-y);
        } else {
          ctx.rotate (t.angle);
        }
      }
      if (t.ttype == DomTransform.Type.MATRIX ||
          t.ttype == DomTransform.Type.SKEWX ||
          t.ttype == DomTransform.Type.SKEWY) {
        if (t.matrix != null) {
          ctx.transform (t.matrix);
        }
      }
    }
  }

  internal void render_line_element (GSvg.DomLineElement l) throws GLib.Error {
    DomLength stroke_width = l.stroke_width_to_lenght ();
    double cstroke_width = 0.0;
    GSvg.DomSvgElement root = null;
    if (l.owner_document is GSvg.Document) {
      root = ((GSvg.Document) l.owner_document).root_element;
    }
    if (root == null) {
      return;
    }
    message ("Checking stroke_width");
    cstroke_width = root.convert_length_x_pixels (stroke_width);
    if (cstroke_width == 0.0) {
      return;
    }
    message ("stroke_width: %g", cstroke_width);
    message ("Checking stroke");
    string stroke = l.get_style_property_value ("stroke");
    if (stroke == null) {
      return;
    }
    message ("stroke: %s", stroke);
    double red, green, blue, alpha;
    red = green = blue = alpha = 0.0;
    parse_color_rgba (stroke, out red, out green, out blue, out alpha);
    ctx.set_source_rgba (red, green, blue, alpha);
    ctx.set_line_width (cstroke_width);
    double x = root.convert_length_x_pixels (l.x1.base_val);
    double y = root.convert_length_y_pixels (l.y1.base_val);
    ctx.save ();
    transform (l, root);
    ctx.move_to (x, y);
    x = root.convert_length_x_pixels (l.x2.base_val);
    y = root.convert_length_y_pixels (l.y2.base_val);
    ctx.line_to (x, y);
    ctx.stroke ();
    ctx.restore ();
  }
  internal void render_circle_element (GSvg.DomCircleElement c) throws GLib.Error {
    DomLength stroke_width = c.stroke_width_to_lenght ();
    double cstroke_width = 0.0;
    GSvg.DomSvgElement root = null;
    if (c.owner_document is GSvg.Document) {
      root = ((GSvg.Document) c.owner_document).root_element;
    }
    if (root == null) {
      return;
    }
    cstroke_width = root.convert_length_x_pixels (stroke_width);

    double red, green, blue, alpha;
    red = green = blue = alpha = 0.0;
    double x = root.convert_length_x_pixels (c.cx.base_val);
    double y = root.convert_length_y_pixels (c.cy.base_val);
    double r = root.convert_length_y_pixels (c.r.base_val);

    string stroke = c.get_style_property_value ("stroke");
    if (stroke != null) {
      parse_color_rgba (stroke, out red, out green, out blue, out alpha);
      ctx.set_source_rgba (red, green, blue, alpha);
    }
    ctx.save ();
    transform (c, root);
    ctx.move_to (x, y);
    ctx.new_sub_path ();
    ctx.arc (x, y, r, 0, 2 * Math.PI);
    string fill = c.get_style_property_value ("fill");
    if (fill == null) {
      ctx.close_path ();
      ctx.fill ();
    } else if (fill.down () != "none") {
      parse_color_rgba (fill, out red, out green, out blue, out alpha);
      ctx.set_source_rgba (red, green, blue, alpha);
      ctx.close_path ();
      ctx.fill ();
    }
    if (cstroke_width != 0.0 && stroke != null) {
      parse_color_rgba (stroke, out red, out green, out blue, out alpha);
      ctx.set_source_rgba (red, green, blue, alpha);
      ctx.set_line_width (cstroke_width);
      ctx.move_to (x, y);
      ctx.new_sub_path ();
      ctx.arc (x, y, r, 0, 2 * Math.PI);
      ctx.stroke ();
    }
    ctx.restore ();
  }
  internal void render_rect_element (GSvg.DomRectElement r) throws GLib.Error {
    DomLength stroke_width = r.stroke_width_to_lenght ();
    double cstroke_width = 0.0;
    GSvg.DomSvgElement root = null;
    if (r.owner_document is GSvg.Document) {
      root = ((GSvg.Document) r.owner_document).root_element;
    }
    if (root == null) {
      return;
    }
    cstroke_width = root.convert_length_x_pixels (stroke_width);

    if (r.x == null || r.y == null || r.width == null || r.height == null) return;
    double red, green, blue, alpha;
    red = green = blue = alpha = 0.0;
    double x = root.convert_length_x_pixels (r.x.base_val);
    double y = root.convert_length_y_pixels (r.y.base_val);
    double width = root.convert_length_y_pixels (r.width.base_val);
    double height = root.convert_length_y_pixels (r.height.base_val);

    string stroke = r.get_style_property_value ("stroke");
    if (stroke != null) {
      parse_color_rgba (stroke, out red, out green, out blue, out alpha);
      ctx.set_source_rgba (red, green, blue, alpha);
    }
    string fill = r.get_style_property_value ("fill");
    bool fb = false;
    if (fill == null) {
      fb = true;
    } else if (fill.down () != "none") {
      fb = true;
    }
    double rx, ry;
    rx = ry = 0.0;
    if (r.rx != null || r.ry != null) {
      if (r.rx != null) {
        rx = root.convert_length_x_pixels (r.rx.base_val);
        if (r.ry == null) {
          ry = rx;
        }
      }
      if (r.ry != null) {
        ry = root.convert_length_y_pixels (r.ry.base_val);
        if (r.rx == null) {
          rx = ry;
        }
      }
      if (rx > width / 2) {
        rx = width / 2;
      }
      if (ry > height / 2) {
        ry = height /2;
      }
    }
    ctx.save ();
    transform (r, root);
    if (fb) {
      if (r.rx != null || r.ry != null) {
        draw_rounded_rectangle (x, y, width, height, rx, ry);
      } else {
        ctx.move_to (x, y);
        ctx.rectangle (x, y, width, height);
      }
      if (fill != null) {
        if (fill.down () != "none") {
          parse_color_rgba (fill, out red, out green, out blue, out alpha);
          ctx.set_source_rgba (red, green, blue, alpha);
        }
      }
      ctx.close_path ();
      ctx.fill ();
    }
    if (stroke != null) {
      parse_color_rgba (stroke, out red, out green, out blue, out alpha);
      ctx.set_source_rgba (red, green, blue, alpha);
      if (r.rx != null || r.ry != null) {
        draw_rounded_rectangle (x, y, width, height, rx, ry);
      } else {
        ctx.move_to (x, y);
        ctx.rectangle (x, y, width, height);
      }
      ctx.set_line_width (cstroke_width);
      ctx.stroke ();
    }
    ctx.restore ();
  }
  private void draw_rounded_rectangle (double x, double y,
                                      double width, double height,
                                      double rx, double ry) {
    ctx.move_to (x + rx, y);
    ctx.line_to (x + width - rx, y);
    ctx.curve_to (x + width - rx, y, x + width, y, x + width, y + ry);
    ctx.line_to (x + width, y + height - ry);
    ctx.curve_to (x + width, y + height - ry, x + width, y + height, x + width - rx, y + height);
    ctx.line_to (x + rx, y + height);
    ctx.curve_to (x + rx, y + height, x, y + height, x, y + height - ry);
    ctx.line_to (x, y + ry);
    ctx.curve_to (x, y + ry, x, y, x + rx, y);
  }
  public void render_ellipse_element (GSvg.DomEllipseElement e) throws GLib.Error {
    DomLength stroke_width = e.stroke_width_to_lenght ();
    double cstroke_width = 0.0;
    GSvg.DomSvgElement root = null;
    if (e.owner_document is GSvg.Document) {
      root = ((GSvg.Document) e.owner_document).root_element;
    }
    if (root == null) {
      return;
    }
    cstroke_width = root.convert_length_x_pixels (stroke_width);
    string stroke = e.get_style_property_value ("stroke");

    if (e.cx == null || e.cy == null || e.rx == null || e.ry == null) return;
    if (e.rx.base_val.value == 0 || e.ry.base_val.value == 0) return;
    double red, green, blue, alpha;
    red = green = blue = 0.0;
    alpha = 1.0;
    double cx = root.convert_length_x_pixels (e.cx.base_val);
    double cy = root.convert_length_y_pixels (e.cy.base_val);
    double rx = root.convert_length_y_pixels (e.rx.base_val);
    double ry = root.convert_length_y_pixels (e.ry.base_val);

    string fill = e.get_style_property_value ("fill");
    bool fb = false;
    if (fill == null) {
      fb = true;
    } else if (fill.down () != "none") {
      fb = true;
    }
    ctx.save ();
    transform (e, root);
    if (fb) {
      if (fill != null) {
        if (fill.down () != "none") {
          parse_color_rgba (fill, out red, out green, out blue, out alpha);
        } else {
          alpha = 0.0;
        }
      }
      ctx.set_source_rgba (red, green, blue, alpha);
      draw_ellipse (cx, cy, rx, ry);
      ctx.fill ();
    }
    if (stroke != null) {
      parse_color_rgba (stroke, out red, out green, out blue, out alpha);
      ctx.set_source_rgba (red, green, blue, alpha);
      ctx.set_line_width (cstroke_width);
      draw_ellipse (cx, cy, rx, ry);
      ctx.stroke ();
    }
    ctx.restore ();
  }
  private void draw_ellipse (double cx, double cy, double rx, double ry) {
    ctx.move_to (cx, cy - ry);
    ctx.curve_to (cx, cy - ry, cx + rx, cy - ry, cx + rx, cy);
    ctx.curve_to (cx + rx, cy, cx + rx, cy + ry, cx, cy + ry);
    ctx.curve_to (cx, cy + ry, cx - rx, cy + ry, cx - rx, cy);
    ctx.curve_to (cx - rx, cy, cx - rx, cy - ry, cx, cy - ry);
  }

  internal void render_polyline_element (GSvg.DomPolylineElement e) throws GLib.Error {
    DomLength stroke_width = e.stroke_width_to_lenght ();
    double cstroke_width = 0.0;
    GSvg.DomSvgElement root = null;
    if (e.owner_document is GSvg.Document) {
      root = ((GSvg.Document) e.owner_document).root_element;
    }
    if (root == null) {
      return;
    }
    cstroke_width = root.convert_length_x_pixels (stroke_width);
    string stroke = e.get_style_property_value ("stroke");

    if (e.points == null) return;
    double red, green, blue, alpha;
    red = green = blue = alpha = 0.0;

    ctx.save ();
    transform (e, root);
    if (stroke != null) {
      parse_color_rgba (stroke, out red, out green, out blue, out alpha);
      ctx.set_source_rgba (red, green, blue, alpha);
      ctx.set_line_width (cstroke_width);
      draw_polyline (e.points);
      ctx.stroke ();
    }
    ctx.restore ();
  }
  private void draw_polyline (GSvg.DomPointList points) {
    if (points.number_of_items == 0) return;
    try {
      DomPoint init = points.get_item (0);
      ctx.move_to (init.x, init.y);
      for (int i = 1; i < points.number_of_items; i++) {
        DomPoint p = points.get_item (i);
        ctx.line_to (p.x, p.y);
      }
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
  }
  internal void render_polygon_element (GSvg.DomPolygonElement e) throws GLib.Error {
    DomLength stroke_width = e.stroke_width_to_lenght ();
    double cstroke_width = 0.0;
    GSvg.DomSvgElement root = null;
    if (e.owner_document is GSvg.Document) {
      root = ((GSvg.Document) e.owner_document).root_element;
    }
    if (root == null) {
      return;
    }
    cstroke_width = root.convert_length_x_pixels (stroke_width);
    string stroke = e.get_style_property_value ("stroke");

    if (e.points == null) return;
    double red, green, blue, alpha;
    red = green = blue = 0.0;
    alpha = 1.0;
    string fill = e.get_style_property_value ("fill");
    bool fb = false;
    if (fill == null) {
      fb = true;
    } else if (fill.down () != "none") {
      fb = true;
    }
    ctx.save ();
    transform (e, root);
    if (fb) {
      if (fill != null) {
        if (fill.down () != "none") {
          parse_color_rgba (fill, out red, out green, out blue, out alpha);
        } else {
          alpha = 0.0;
        }
      }
      ctx.set_source_rgba (red, green, blue, alpha);
      draw_polyline (e.points);
      ctx.close_path ();
      ctx.fill ();
    }
    if (stroke != null) {
      parse_color_rgba (stroke, out red, out green, out blue, out alpha);
      ctx.set_source_rgba (red, green, blue, alpha);
      ctx.set_line_width (cstroke_width);
      draw_polyline (e.points);
      ctx.close_path ();
      ctx.stroke ();
    }
    ctx.restore ();
  }
  internal void render_text_element (GSvg.DomTextElement t, GSvg.DomSvgElement e) throws GLib.Error {
    DomLength stroke_width = e.stroke_width_to_lenght ();
    double cstroke_width = 0.0;
    GSvg.DomSvgElement root = null;
    if (e.owner_document is GSvg.Document) {
      root = ((GSvg.Document) e.owner_document).root_element;
    }
    if (root == null) {
      return;
    }
    cstroke_width = root.convert_length_x_pixels (stroke_width);
    string stroke = e.get_style_property_value ("stroke");

    DomAnimatedLength fs = new GSvg.AnimatedLength ();
    string fm = t.get_style_property_value ("font-family");
    if (fm == null) {
      return;
    }
    string sfs = t.get_style_property_value ("font-size");
    if (sfs == null) {
      return;
    }
    fs.value = sfs;
    string fill = t.get_style_property_value ("fill");
    if (fill == null && stroke == null) {
      return;
    }
    Pango.Style fst = Pango.Style.NORMAL;
    string fstyle = t.get_style_property_value ("font-style");
    if (fstyle != null) {
      switch (fstyle.down ()) {
        case "normal":
        break;
        case "italic":
          fst = Pango.Style.ITALIC;
        break;
        case "oblique":
          fst = Pango.Style.OBLIQUE;
        break;
        case "inherit":
        break;
      }
    }
    Pango.Weight fweight = Pango.Weight.NORMAL;
    string fw = t.get_style_property_value ("font-weight");
    if (fw != null) {
      switch (fw.down ()) {
        case "normal":
        break;
        case "bold":
          fweight = Pango.Weight.BOLD;
        break;
        case "bolder":
          fweight = Pango.Weight.ULTRABOLD;
        break;
        case "lighter":
          fweight = Pango.Weight.SEMILIGHT;
        break;
        case "100":
          fweight = Pango.Weight.THIN;
        break;
        case "200":
          fweight = Pango.Weight.ULTRALIGHT;
        break;
        case "300":
          fweight = Pango.Weight.LIGHT;
        break;
        case "400":
          fweight = Pango.Weight.NORMAL;
        break;
        case "500":
          fweight = Pango.Weight.MEDIUM;
        break;
        case "600":
          fweight = Pango.Weight.SEMIBOLD;
        break;
        case "700":
          fweight = Pango.Weight.BOLD;
        break;
        case "800":
          fweight = Pango.Weight.ULTRABOLD;
        break;
        case "900":
          fweight = Pango.Weight.HEAVY;
        break;
        case "1000":
          fweight = Pango.Weight.ULTRAHEAVY;
        break;
        case "inherit":
        break;
      }
    }
    Pango.Alignment faligment = Pango.Alignment.LEFT;
    string fa = t.get_style_property_value ("text-anchor");
    if (fa != null) {
      switch (fa.down ()) {
        case "start":
          faligment = Pango.Alignment.LEFT;
        break;
        case "middle":
          faligment = Pango.Alignment.CENTER;
        break;
        case "end":
          faligment = Pango.Alignment.RIGHT;
        break;
      }
    }
    double fsd = root.convert_length_y_pixels (fs.base_val);
    var fd = new Pango.FontDescription ();
    fd.set_family (fm);
    fd.set_absolute_size ((int) fsd * Pango.SCALE);
    fd.set_style (fst);
    fd.set_weight (fweight);
    var pl = ctx.create_layout ();
    pl.set_font_description (fd);
    pl.set_width (-1);
    pl.set_height (0);
    pl.set_ellipsize (Pango.EllipsizeMode.NONE);
    pl.set_justify (false);
    pl.set_alignment (faligment);
    double red, green, blue, alpha;
    red = green = blue = alpha = 0.0;
    double x = 0.0;
    double y = 0.0;
    string txt = t.text_content;
    int pos = 0;
    bool moved = false;
    ctx.save ();
    transform (t, root);
    for (int i = 0; i < txt.length; i++) {
      if (t.x != null) {
        if (i < t.x.base_val.number_of_items) {
          x = root.convert_length_x_pixels (t.x.base_val.get_item (i));
          if (i + 1 < t.x.base_val.number_of_items) {
            moved = true;
          }
        }
      }
      if (t.y != null) {
        if (i < t.y.base_val.number_of_items) {
          y = root.convert_length_y_pixels (t.y.base_val.get_item (i));
          if (i + 1 < t.y.base_val.number_of_items) {
            moved = true;
          }
        }
      }
      string tx = txt.get_char (i).to_string ();
      if (!moved) {
        tx = txt.substring (i, -1);
      }
      pl.set_text (tx, -1);
      int tw = 0;
      int th = 0;
      if (fill != null) {
        parse_color_rgba (fill, out red, out green, out blue, out alpha);
        ctx.set_source_rgba (red, green, blue, alpha);
      }
      ctx.update_layout (pl);
      pl.get_pixel_size (out tw, out th);
      double al = 0.0;
      if (faligment == Pango.Alignment.RIGHT) {
        al = - tw;
      }
      if (faligment == Pango.Alignment.CENTER) {
        al = - tw / 2;
      }
      ctx.move_to (x + pos + al, y - fsd);
      if (fill != null) {
        ctx.show_layout (pl);
      }
      if (stroke != null && cstroke_width != 0.0) {
        parse_color_rgba (stroke, out red, out green, out blue, out alpha);
        ctx.set_source_rgba (red, green, blue, alpha);
        ctx.set_line_width (cstroke_width);
        ctx.layout_path (pl);
        ctx.stroke ();
      }
      pos += tw;
      if (!moved) {
        break;
      }
      moved = false;
    }
    ctx.restore ();
  }
}
