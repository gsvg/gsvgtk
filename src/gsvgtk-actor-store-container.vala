/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor-store-container.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gee;

public class GSvgtk.ActorStoreContainer : Gee.HashMap<string,Actor>, GLib.ListModel, ActorStore
{
    private TreeMap<uint,Actor> index = new TreeMap<int,Actor> ();
    private TreeMap<string,uint> index_hashed = new TreeMap<string,uint> ();

    public new void @set (string k, Actor actor) { add (k, actor); }
    public new void unset (string k) { remove (k); }

    public void add (string id, Actor actor) {
        ((Gee.HashMap<string,Actor>) this).set (id, actor);
        index.set (size - 1, actor);
        index_hashed.@set (id, size - 1);
    }

    public void remove (string id) {
      if (!has_key (id)) {
        return;
      }
      unset (id);
      index.unset (index_hashed.get (id));
      index_hashed.unset (id);
    }
    public Actor? find (string id) {
      return get (id);
    }

    public Object? get_item (uint position) {
      if (!index.has_key (position)) {
        return null;
      }
      return index.get (position) as Object;
    }
    public Type get_item_type () {
      return typeof (Actor);
    }
    public uint get_n_items () {
      return (uint) size;
    }
    public Object? get_object (uint position) {
      return get_item (position);
    }
}

