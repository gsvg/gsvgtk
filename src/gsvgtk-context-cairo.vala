/* gsvgtk-actor-shape-clutter.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Pango;

public class GSvgtk.ContextCairo : GLib.Object, GSvgtk.Context {
    private Cairo.Context ctx = null;
    private GSvgtk.Surface _surface = null;
    public GSvgtk.Surface surface {
        get {
            return _surface;
        }
    }
    public ContextCairo (Cairo.Context cx) {
        ctx = cx;
        _surface = new GSvgtk.SurfaceCairo (cx.get_target ()) as GSvgtk.Surface;
    }
    public Cairo.Context get_cairo_context () { return ctx; }
    public void save () { ctx.save (); }
    public void restore () { ctx.restore (); }
    public void translate (double x, double y) { ctx.translate (x, y); }
    public void scale (double x, double y) { ctx.scale (x, y); }
    public void rotate (double angle) { ctx.rotate (angle); }
    public void transform (GSvg.DomMatrix m) {
        var mt = Cairo.Matrix (m.a, m.b, m.c, m.d, m.e, m.f);
        ctx.transform (mt);
    }
    public void set_source_rgba (double red, double green,
                              double blue, double alpha) {
        ctx.set_source_rgba (red, green, blue, alpha);
    }
    public void set_line_width (double w) { ctx.set_line_width (w); }
    public void move_to (double x, double y) { ctx.move_to (x, y); }
    public void line_to (double x, double y) { ctx.line_to (x, y); }
    public void stroke () { ctx.stroke (); }
    public void new_sub_path () { ctx.new_sub_path (); }
    public void arc (double x, double y, double r, double angle1, double angle2) {
        ctx.arc (x, y, r, angle1, angle2);
    }
    public void curve_to (double x1, double y1, double x2, double y2, double x3, double y3) {
        ctx.curve_to (x1, y1, x2, y2, x3, y3);
    }
    public void close_path () { ctx.close_path (); }
    public void fill ()  { ctx.fill (); }
    public void rectangle (double x, double y, double width, double height) {
    ctx.rectangle (x, y, width, height);
    }
    public Pango.Layout create_layout () { return Pango.cairo_create_layout (ctx); }
    public void update_layout (Pango.Layout pl) { Pango.cairo_update_layout (ctx, pl); }
    public void show_layout (Pango.Layout pl) { Pango.cairo_show_layout (ctx, pl); }
    public void layout_path (Pango.Layout pl) { Pango.cairo_layout_path (ctx, pl); }
}
